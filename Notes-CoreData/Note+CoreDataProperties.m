//
//  Note+CoreDataProperties.m
//  Notes-CoreData
//
//  Created by APPLE on 25/02/16.
//  Copyright © 2016 APPLE. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Note+CoreDataProperties.h"

@implementation Note (CoreDataProperties)

@dynamic title;
@dynamic desc;

@end
