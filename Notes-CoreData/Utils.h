//
//  Utils.h
//  Notes-CoreData
//
//  Created by APPLE on 25/02/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Utils : NSObject

+ (CGSize)sizeForString:(NSString *)string maxWidth:(CGFloat)maxWidth attributes:(NSDictionary *)attributes;
+ (CGFloat)heightForString:(NSString *)string maxWidth:(CGFloat)maxWidth font:(UIFont *)font;

@end
