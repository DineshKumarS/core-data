//
//  NoteTableViewCell.h
//  Notes-CoreData
//
//  Created by APPLE on 25/02/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@end
