//
//  AddNoteViewController.m
//  Notes-CoreData
//  Created by APPLE on 25/02/16.
//  Copyright © 2016 APPLE. All rights reserved.

#import "AddNoteViewController.h"
#import "Note.h"
#import "AppDelegate.h"
#import "ViewController.h"

@interface AddNoteViewController ()
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextView *descTextView;

@end

@implementation AddNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)saveClicked:(id)sender {
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Note" inManagedObjectContext:appDelegate.managedObjectContext];
    Note *note = (Note *)[[NSManagedObject alloc]initWithEntity:entityDesc insertIntoManagedObjectContext:appDelegate.managedObjectContext];
    note.title = self.titleTextField.text;
    note.desc = self.descTextView.text;
    [appDelegate saveContext];
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
