//
//  Utils.m
//  Notes-CoreData
//
//  Created by APPLE on 25/02/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (CGSize)sizeForString:(NSString *)string maxWidth:(CGFloat)maxWidth attributes:(NSDictionary *)attributes {
    CGSize constraint = CGSizeMake(maxWidth,NSUIntegerMax);
    CGRect rect = [string boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)attributes:attributes context:nil];
    rect.size.width = rect.size.width+15;
    return rect.size;
}

+ (CGFloat)heightForString:(NSString *)string maxWidth:(CGFloat)maxWidth font:(UIFont *)font {
    CGSize constraint = CGSizeMake(maxWidth,NSUIntegerMax);
    
    NSDictionary *attributes = @{NSFontAttributeName: font};
    
    CGRect rect = [string boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)attributes:attributes context:nil];
    return rect.size.height;
}


@end
