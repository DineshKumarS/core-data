//
//  ViewController.m
//  Notes-CoreData
//
//  Created by APPLE on 25/02/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import "ViewController.h"
#import "NoteTableViewCell.h"
#import "Note.h"
#import "Utils.h"
#import "AppDelegate.h"
#import "AddNoteViewController.h"

@interface ViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong)  NSArray *dataArray;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self reloadData];
}

- (void)reloadData {
    // Fetch from coredata and reloadTable
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Note"];
    self.dataArray = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)add:(id)sender {
    AddNoteViewController *addNoteVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AddNoteVC"];
    [self.navigationController pushViewController:addNoteVC animated:YES];
}

#pragma mark - UITableViewDelegate and Datasource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NoteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    Note *note = [self.dataArray objectAtIndex:indexPath.row];
    cell.titleLabel.text = note.title;
    cell.descLabel.text = note.desc;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Note *note = [self.dataArray objectAtIndex:indexPath.row];
    [tableView layoutIfNeeded];
    CGFloat height = 21;
    height = height + [Utils heightForString:note.title maxWidth:tableView.frame.size.width - 16 font:[UIFont systemFontOfSize:17]];
    height = height + [Utils heightForString:note.desc maxWidth:tableView.frame.size.width - 16 font:[UIFont systemFontOfSize:17]];
    return height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

@end
